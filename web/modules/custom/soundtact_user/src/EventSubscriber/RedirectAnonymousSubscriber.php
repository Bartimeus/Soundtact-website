<?php

namespace Drupal\soundtact_user\EventSubscriber;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class RedirectAnonymousSubscriber implements EventSubscriberInterface {

  const FORCE_EXCLUSION_PREFIX = '~';

  /**
   * The user who is requesting a page.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Class to check the excluded routes.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Array with routes that should be excluded from redirect.
   *
   * @var array
   */
  protected $excludedRoutes = [
    'user.login',
    'user.reset',
    'user.pass',
    'user.register',
    'user.reset.*',
    'rest.*',
    'soundtact.api.*',
    'oauth2_token.token',
    'system.temporary',
  ];

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current logged in user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Class to check the excluded routes.
   */
  public function __construct(AccountProxyInterface $account, RouteMatchInterface $routeMatch) {
    $this->account = $account;
    $this->routeMatch = $routeMatch;
  }

  /**
   * Check if user has permissions to view the content else redirect.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Response event.
   */
  public function checkAuthStatus(RequestEvent $event) {
    $route_name = $this->routeMatch->getRouteName();

    // When user is authenticated, the user can access.
    if ($route_name === NULL || $this->account->isAuthenticated()) {
      return;
    }

    // Check if route matches with excluded routes.
    foreach ($this->excludedRoutes as $excluded_route) {
      if (preg_match('/' . $excluded_route . '/', $route_name)) {
        // Check if the route should is forced to redirect.
        if (!in_array(static::FORCE_EXCLUSION_PREFIX . $route_name, $this->excludedRoutes, TRUE)) {
          return;
        }
      }
    }

    $response = new RedirectResponse('/user/login');
    $event->setResponse($response);
    $event->stopPropagation();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkAuthStatus'];
    $events[KernelEvents::EXCEPTION][] = ['checkAuthStatus'];
    return $events;
  }

}
