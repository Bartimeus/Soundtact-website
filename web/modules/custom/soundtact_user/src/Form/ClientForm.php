<?php

namespace Drupal\soundtact_user\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EditRouteForm.
 */
class ClientForm extends FormBase {

  const MODULE_NAME = 'soundtact_user';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *   Entity type manager object.
   */
  protected $entityTypeManager;

  /**
   * The user storage to retrieve users.
   *
   * @var \Drupal\user\UserStorageInterface
   *   User storage.
   */
  protected $userStorage;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   *  Taxonomy term storage.
   */
  protected $taxonomyStorage;


  /**
   * The current logged in user.
   *
   * @var \Drupal\user\Entity\User
   *   User.
   */
  protected $currentUser;

  /**
   * The user which is being edited.
   *
   * @var \Drupal\user\Entity\User
   *   User.
   */
  protected $editingUser;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Logger interface.
   */
  protected $logger;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   *   renderer interface.
   */
  protected $renderer;

  /**
   * ClientForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer to render a view.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger, RendererInterface $renderer) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->renderer = $renderer;
    $this->userStorage = $this->entityTypeManager->getStorage('user');
    $this->taxonomyStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->currentUser = $this->userStorage->load($this->currentUser()->id());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('renderer')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'client_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\user\UserInterface|null $user
   *   The user when editing.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    if ($user !== NULL) {
      $this->editingUser = $user;
    }

    $first_name = $form_state->get('first_name');
    if ($user !== NULL && $field_first_name = $user->field_firstname) {
      $first_name = $field_first_name->getValue()[0]['value'];
    }

    $form_title = $this->t('Edit client');
    if ($this->editingUser === NULL) {
      $form_title = $this->t('Create client');
    }

    $form['title'] = [
      '#markup' => '<h2>' . $form_title . '</h2>',
    ];

    $form['first_name'] = [
      '#title' => $this->t('First name'),
      '#placeholder' => $this->t('First name'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $first_name,
    ];

    $last_name = $form_state->get('last_name');
    if ($user !== NULL && $field_last_name = $user->field_lastname) {
      $last_name = $field_last_name->getValue()[0]['value'];
    }

    $form['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
      '#placeholder' => $this->t('Last name'),
      '#required' => TRUE,
      '#default_value' => $last_name,
    ];

    if (soundtact_user_is_admin($this->currentUser)) {
      // Get all themes.
      $term_list = $this->taxonomyStorage->loadTree('theme');
      $terms = [];

      foreach ($term_list as $term) {
        $terms[$term->tid] = $term->name;
      }

      $default_term_value = [];
      if ($user) {
        foreach ($user->field_theme as $theme) {
          $default_term_value[] = $theme->getValue()['target_id'];
        }
      }

      $form['sidebar']['header']['theme'] = [
        '#type' => 'select',
        '#title' => $this->t('Thema'),
        '#options' => $terms,
        '#multiple' => 'multiple',
        '#default_value' => $default_term_value,
      ];
    }

    $has_password = FALSE;
    if ($user !== NULL && $user->field_has_password && $user->field_has_password->getValue()[0]['value']) {
      $has_password = TRUE;
    }

    $form['has_password'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cliënt can login'),
      '#description' => $this->t('If this box is checked you can assign a username and password to the client. The user can use this information to login to the application.'),
      '#default_value' => $has_password,
    ];

    $username = '';
    if ($user != NULL) {
      $username = $user->name->getValue()[0]['value'];
    }

    $form['password_wrapper']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $username,
      '#states' => [
        'visible' => [
          ':input[name="has_password"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['password_wrapper']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#states' => [
        'visible' => [
          ':input[name="has_password"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['password_wrapper']['password_repeat'] = [
      '#type' => 'password',
      '#title' => $this->t('Confirm password'),
      '#states' => [
        'visible' => [
          ':input[name="has_password"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $submit_title = $this->t('Save changes');
    if ($this->editingUser === NULL) {
      $submit_title = $this->t('Create user');
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#attributes' => ['class' => ['button--primary', 'button']],
      '#value' => $submit_title,
    ];

    if ($this->editingUser !== NULL) {
      /** @var \Drupal\views\ViewExecutable $view */
      $view = $this->entityTypeManager
        ->getStorage('view')
        ->load('client_route_overview')
        ->getExecutable();

      $view->setArguments([$this->editingUser->id()]);

      $excution_view_display = $view->executeDisplay('block_1');

      $form['route_view']['title'] = [
        '#markup' => '<h2>' . $this->t('Added routes') . '</h2>',
      ];

      $form['route_view']['view'] = [
        '#markup' => $this->renderer->render($excution_view_display),
      ];
    }

    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check if has_password is checked.
    if (!$form_state->getValue('has_password')) {
      return;
    }

    // If the 2 password field are empty continue with submitting.
    // The password has not changed so we shouldn't do anything.
    $password = $form_state->getValue('password');
    $password_repeat = $form_state->getValue('password_repeat');

    if (empty($form_state->getValue('username'))) {
      $form_state->setErrorByName('username', $this->t('Username cannot be empty'));
    }

    if ($form_state->getFormObject()->editingUser !== NULL && (empty($password) && empty($password_repeat))) {
      return;
    }

    if (empty($password)) {
      $form_state->setErrorByName('password', $this->t('Password should not be empty.'));
    }

    if (empty($password_repeat)) {
      $form_state->setErrorByName('password_repeat', $this->t('Password repeat should not be empty.'));
    }

    // If the 2 password fields aren't empty check if they are the same.
    if ($password != $password_repeat) {
      $form_state->setErrorByName('password', $this->t('Passwords are not the same.'));
      $form_state->setErrorByName('password_repeat', $this->t('Passwords are not the same.'));
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // First check if we are editing the user.
    if (!empty($form_state->getFormObject()->editingUser)) {
      /** @var \Drupal\user\UserInterface $editing_user */
      $editing_user = $form_state->getFormObject()->editingUser;

      try {
        $editing_user->set('name', $values['username']);
        $editing_user->set('field_firstname', $values['first_name']);
        $editing_user->set('field_lastname', $values['last_name']);

        if ($values['has_password']) {
          $editing_user->set('field_has_password', TRUE);
        }
        else {
          $editing_user->set('field_has_password', FALSE);
        }

        if ($values['has_password'] && (!empty($values['password'] && !empty($values['password_repeat'])))) {
          $editing_user->setPassword($values['password']);
        }

        if (soundtact_user_is_admin($this->currentUser)) {
          $editing_user->set('field_theme', $values['theme']);
        }

        $editing_user->save();
      }
      catch (\Exception $exception) {
        $this->messenger()->addError($this->t('The client could not be edited. Please contact a administrator.'));
        $this->logger->get(self::MODULE_NAME)->error($exception->getMessage());
        return;
      }

      $this->messenger()->addStatus($this->t('The client has succesfully been edited.'));
      $redirect_url = Url::fromRoute('view.client_overzicht.page_1');

      // We do this instead of setting the form_state, because the
      // destination overwrites the redirect that's being set in
      // the form_state.
      $this->getRequest()->query->set('destination', '/' . $redirect_url->getInternalPath());
    }
    // We are creating a new user.
    else {
      $current_users_theme_ids = [];
      // First get all the themes from the current logged in user.
      foreach ($this->currentUser->field_theme->getValue() as $theme) {
        $current_users_theme_ids[] = $theme['target_id'];
      }

      $first_name = $values['first_name'];
      $last_name = $values['last_name'];

      $user_array = [
        'status' => TRUE,
        'field_firstname' => $first_name,
        'field_lastname' => $last_name,
        'name' => $this->generateUniqueUsername($first_name, $last_name),
        'field_theme' => $current_users_theme_ids,
      ];

      if (soundtact_user_is_admin($this->currentUser)) {
        $user_array['field_theme'] = $values['theme'];
      }

      if (!empty($values['username'])) {
        $user_array['name'] = $values['username'];
      }

      if ($values['has_password']) {
        $user_array['field_has_password'] = TRUE;
        $user_array['password'] = $values['password'];
      }
      else {
        $user_array['field_has_password'] = FALSE;
      }

      $user = User::create($user_array);
      $user->addRole('user');

      try {
        $user->save();
      }
      catch (\Exception $exception) {
        $this->messenger()->addError($this->t('The client could not be created. Please contact a admin.'));
        $form_state->setRebuild();
        return;
      }

      $this->messenger()->addStatus($this->t('The client has succesfully been created.'));
      $redirect_url = Url::fromRoute('view.client_overzicht.page_1');
      $form_state->setRedirectUrl($redirect_url);
    }
  }

  /**
   * Generates a unique name based on the first name and last name.
   *
   * @param string $firstname
   *   The first name of the client.
   * @param string $lastname
   *   The last name of the client.
   *
   * @return string
   *   The username generated.
   */
  private function generateUniqueUsername(string $firstname = '', string $lastname = '') {
    $name = strtolower(trim($firstname . ' ' . $lastname));
    $i = 0;
    do {
      $query = $this->userStorage->getQuery();
      $query->condition('name', (++$i) == 1 ? $name : $name . $i);
      $matches = $query->execute();
    } while (count($matches) > 0);
    if ($i > 1) {
      $name .= $i;
    }
    return $name;
  }

}
