<?php

namespace Drupal\soundtact_beacon\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Beacon entities.
 */
class BeaconEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
