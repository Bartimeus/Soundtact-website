<?php

namespace Drupal\soundtact_beacon\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Beacon entities.
 *
 * @ingroup soundtact_beacon
 */
interface BeaconEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Beacon name.
   *
   * @return string
   *   Name of the Beacon.
   */
  public function getName();

  /**
   * Sets the Beacon name.
   *
   * @param string $name
   *   The Beacon name.
   *
   * @return \Drupal\soundtact_beacon\Entity\BeaconEntityInterface
   *   The called Beacon entity.
   */
  public function setName($name);

  /**
   * Gets the Beacon creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Beacon.
   */
  public function getCreatedTime();

  /**
   * Sets the Beacon creation timestamp.
   *
   * @param int $timestamp
   *   The Beacon creation timestamp.
   *
   * @return \Drupal\soundtact_beacon\Entity\BeaconEntityInterface
   *   The called Beacon entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Beacon published status indicator.
   *
   * Unpublished Beacon are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Beacon is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Beacon.
   *
   * @param bool $published
   *   TRUE to set this Beacon to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\soundtact_beacon\Entity\BeaconEntityInterface
   *   The called Beacon entity.
   */
  public function setPublished($published);

}
