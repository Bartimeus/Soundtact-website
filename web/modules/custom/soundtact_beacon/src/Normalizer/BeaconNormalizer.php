<?php

namespace Drupal\soundtact_beacon\Normalizer;

use Drupal\soundtact_api\Normalizer\ContentEntityNormalizer;
use Drupal\soundtact_beacon\Entity\BeaconEntityInterface;

/**
 * Converts the Drupal entity object structures to a normalized array.
 */
class BeaconNormalizer extends ContentEntityNormalizer {

  /**
   * The normalizer used to normalize the typed data.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    // If we aren't dealing with an object or the format is not supported return
    // now.
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }

    if ($data instanceof BeaconEntityInterface) {
      return TRUE;
    }
    // Otherwise, this normalizer does not support the $data object.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    // Can't inject serializer due to the following error:
    // [error]  Circular reference detected for service "serializer",
    // path: "serializer -> soundtact_api.typed_data -> serializer".
    // @todo Fix this error in core?
    // @codingStandardsIgnoreStart
    /* @var $object \Drupal\Core\TypedData\TypedDataInterface */
    if (!$this->serializer) {
      $this->serializer = \Drupal::service('serializer');
    }
    // @codingStandardsIgnoreEnd

    $geolocation_field = $entity->geolocation_field->getValue()[0];

    unset($entity->geolocation_field);

    $normalized_beacon = parent::normalize($entity, $format, $context);

    $normalized_beacon['geolocation_field'] = $geolocation_field;

    // Set lat and lng to floats.
    $normalized_beacon['geolocation_field']['lat'] = (float) $normalized_beacon['geolocation_field']['lat'];
    $normalized_beacon['geolocation_field']['lng'] = (float) $normalized_beacon['geolocation_field']['lng'];

    unset($normalized_beacon['user_id']);

    return $normalized_beacon;
  }

}
