/**
 * @file
 */

(function ($, Drupal, drupalSettings) {
  "use strict";

  // The path to the marker icon when the beacon is added.
  var MARKER_ADDED_ICON =
    "/modules/custom/soundtact_route/images/map-marker-added.png";

  var LEAFLET_ADDED_ICON = L.Icon.extend({
    options: {
      iconUrl: MARKER_ADDED_ICON,
      iconSize: [34, 38]
    }
  });

  // The path to the marker icon when beacon is not yet added.
  var MARKER_NEW_ICON =
    "/modules/custom/soundtact_route/images/map-marker-new.png";

  var LEAFLET_NEW_ICON = L.Icon.extend({
    options: {
      iconUrl: MARKER_NEW_ICON,
      iconSize: [34, 38]
    }
  });

  var routeId = drupalSettings.soundtact_route.routeId || "";

  /**
   * Soundtact functionality.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches geocoder functionality to relevant elements.
   */
  Drupal.behaviors.soundtactFunctionality = {
    attach: function (context, drupalSettings) {
      // Initialize all markers.
      $(".geolocation-map-wrapper").each(function (index, item) {
        var mapWrapper = $(item);
        var mapSettings = {};
        mapSettings.id = mapWrapper.attr("id");
        mapSettings.wrapper = mapWrapper;

        // Get the map object so we can set some callbacks.
        var map = Drupal.geolocation.maps[0];
        map.addInitializedCallback(function (initializedMap) {
          // Invoking this function because it renders all the markers.
          onBoundsChanged(initializedMap);

          initializedMap.leafletMap.on("moveend", function (e) {
            onBoundsChanged(initializedMap);
          });

          initializedMap.addMarkerAddedCallback(function (marker) {
            var newContentString =
              '<div class="marker-content">' +
              "<h2>" +
              marker.options.title +
              "</h2>" +
              '<button type="button" data-beacon-id="' +
              marker.options.id +
              '" onclick="addBeacon(event)">' +
              Drupal.t("Add") +
              "</button></div>";

            var addedContentString =
              '<div class="marker-content">' +
              "<h2>" +
              marker.options.title +
              "</h2></div>";

            if (isBeaconAdded(marker.options.id)) {
              marker.setIcon(new LEAFLET_ADDED_ICON());
              marker.closePopup();
              marker.bindPopup(addedContentString);
            }
            else {
              marker.setIcon(new LEAFLET_NEW_ICON());
              marker.bindPopup(newContentString);
            }
          }, true);
        });
      });
    },
    detach: function (context, drupalSettings) {
      // TODO: Check if we should detach something.
    }
  };

  // Each time a ajax command is completed we
  // want to get the markers that are added to rerender
  // the map.
  Drupal.behaviors.onAjaxComplete = {
    attach: function (context, drupalSettings) {
      $(document).ajaxComplete(function (event, xhr, settings) {
        if (settings.type === "POST") {
          $(".geolocation-map-wrapper").each(function (index, item) {
            var mapWrapper = $(item);
            var mapSettings = {};
            mapSettings.id = mapWrapper.attr("id");
            mapSettings.wrapper = mapWrapper;

            var map = Drupal.geolocation.Factory(mapSettings, false);

            $.each(map.mapMarkers, function (markerKey, marker) {
              if (isBeaconAdded(marker.options.id)) {
                marker.setIcon(new LEAFLET_ADDED_ICON());
              }
else {
                marker.setIcon(new LEAFLET_NEW_ICON());
              }
            });
          });
        }
      });
    }
  };

  function getBeacons(routeId, northEastBounds, southWestBounds) {
    return $.ajax("/api/beacons/" + routeId, {
      data: {
        latMax: northEastBounds.lat,
        latMin: southWestBounds.lat,
        lngMax: northEastBounds.lng,
        lngMin: southWestBounds.lng
      }
    }).done(function (data) {
      if (data && data.beacons) {
        return data;
      }
else {
        throw "Something went wrong with retrieving the beacons";
      }
    });
  }

  function onBoundsChanged(map) {
    var bounds = map.leafletMap.getBounds();
    var northEastBounds = bounds.getNorthEast();
    var southWestBounds = bounds.getSouthWest();

    // Get marker with the given bounds.
    getBeacons(routeId, northEastBounds, southWestBounds)
      .then(function (data) {
        var newBeacons = [];
        var markersToRemove = [];
        var mapMarkers = map.mapMarkers;

        // Check if the beacon should be rendered.
        $.each(data.beacons, function (beaconKey, beacon) {
          var newBeacon = true;
          $.each(mapMarkers, function (markerKey, marker) {
            if (marker.options.id === beacon.id) {
              newBeacon = false;
            }
          });

          if (newBeacon) {
            newBeacons.push(beacon);
            return true;
          }
        });

        // Check if the beacon should be removed.
        $.each(mapMarkers, function (markerKey, marker) {
          var deleteMarker = true;
          $.each(newBeacons, function (newBeaconKey, newBeacon) {
            if (marker && newBeacon.id === marker.options.id) {
              deleteMarker = false;
            }
          });

          $.each(data.beacons, function (beaconKey, beacon) {
            if (marker && beacon.id === marker.options.id) {
              deleteMarker = false;
            }
          });

          // We cant delete the marker from the foreach
          // because that will break the foreach. :-)
          if (deleteMarker) {
            markersToRemove.push(marker);
          }
        });

        $.each(markersToRemove, function (markerKey, marker) {
          map.removeMapMarker(marker);
        });

        // We got new beacons so we want to render them on the map.
        renderBeaconsAsMarkers(map, newBeacons);
      })
      .catch(function (e) {
        console.error(e);
      });
  }

  function renderBeaconsAsMarkers(map, beacons) {
    // List of beacons we should render on the map.
    var beaconMarkers = [];

    $.each(beacons, function (beaconKey, beacon) {
      var markerSettings = createBeaconMarker(beacon);
      map.setMapMarker(markerSettings);
    });
  }

  function createBeaconMarker(beacon) {
    var icon = MARKER_NEW_ICON;

    return {
      position: {
        lat: beacon.lat,
        lng: beacon.lng
      },
      title: beacon.name,
      icon: icon,
      id: beacon.id
    };
  }

  /**
   * Check if the beacon is added to the beacons in the route form.
   *
   * @param {int} beaconId
   *
   * @return {boolean}
   */
  function isBeaconAdded(beaconId) {
    // First check if the beacon.
    var beacon_ids = [];
    $.each($(".beacon-id"), function (key, beacon) {
      beacon_ids.push(parseInt($(beacon).val()));
    });
    return beacon_ids.indexOf(beaconId) != -1;
  }
})(jQuery, Drupal, drupalSettings);

/**
 * This function is called when you click on the add button in a marker.
 *
 * @param e
 *   Event.
 */
function addBeacon(e) {
  var beaconId = jQuery(e.target).data("beacon-id");
  if (beaconId !== undefined) {
    var beacon_store = jQuery("#edit-map-beacon");
    beacon_store.val(beaconId);

    var button = jQuery(".sidebar-add-point");
    button.trigger("mousedown");
  }
}
