<?php

namespace Drupal\soundtact_route\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form which is used to create a route.
 */
class EditRouteForm extends FormBase {
  use DialogFormHelper;
  use MusicDialogFormHelper;

  const MODULE_NAME = 'soundtact_route';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *   Entity type manager object.
   */
  protected $entityTypeManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   *   Entity storage.
   */
  protected $nodeStorage;

  /**
   * The beacon entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   *   Entity storage.
   */
  protected $beaconStorage;

  /**
   * The media entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   *   Entity storage.
   */
  protected $mediaStorage;

  /**
   * The file entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   *   Entity storage.
   */
  protected $fileStorage;

  /**
   * The points entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   *   Entity storage.
   */
  protected $pointStorage;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   *  Taxonomy term storage.
   */
  protected $taxonomyStorage;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\user\UserStorageInterface
   *  Taxonomy term storage.
   */
  protected $userStorage;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Logger interface.
   */
  protected $logger;

  /**
   * The logger.
   *
   * @var \Drupal\user\UserInterface
   *   Logger interface.
   */
  protected $currentUser;

  /**
   * EditRouteForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->taxonomyStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->pointStorage = $entity_type_manager->getStorage('point');
    $this->beaconStorage = $entity_type_manager->getStorage('beacon_entity');
    $this->mediaStorage = $entity_type_manager->getStorage('media');
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->userStorage = $entity_type_manager->getStorage('user');
    $user_id = $this->currentUser()->id();
    $this->currentUser = $this->userStorage->load($user_id);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'route_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $route = NULL) {
    $editing = $route === NULL ? FALSE : TRUE;

    $form += [
      '#prefix' => '<div id="form-wrapper">',
      '#suffix' => '</div>',
    ];

    // Create basic form structure.
    $form['sidebar'] = [
      '#type' => 'container',
      '#attached' => [
        'library' => ['soundtact_route/soundtact_route-form'],
      ],
      '#prefix' => '<div id="sidebar-wrapper">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => [
          'sidebar',
        ],
      ],
    ];

    $this->setupDialog($form, $form_state);
    $this->setupMusicForm($form, $form_state);

    $form['map'] = [
      '#type' => 'container',
    ];

    $form['sidebar']['info_header'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'route-info-header',
        ],
      ],
    ];

    $form['sidebar']['info_header']['back_to_overview'] = [
      '#type' => 'link',
      '#title' => $this->t('Back to overview'),
      '#url' => Url::fromRoute('view.route_overview.page_1'),
    ];

    $form['sidebar']['info_header']['body'] = [
      '#markup' => '<p>' . $this->t('Add a description about what a administrator should do when creating a route.') . '</p>',
    ];

    // Create header of sidebar.
    $form['sidebar']['header'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'route-header',
        ],
      ],
    ];

    if ($form_state->get('route_title') === NULL) {
      $form_state->set('route_title', '');
    }

    $title = $route ? $route->getTitle() : $form_state->get('route_title');

    $form['sidebar']['header']['route_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Route title:'),
      '#default_value' => $title,
      '#placeholder' => $this->t('Route name'),
    ];

    $route_id = $route ? $route->id() : '';

    $form['sidebar']['header']['route_id'] = [
      '#type' => 'hidden',
      '#value' => $route_id,
    ];

    $default_users = NULL;
    if ($route && $route->field_clients && !empty($route->field_clients->getValue())) {
      $clients_ids = [];
      foreach ($route->field_clients->getValue() as $client) {
        $clients_ids[] = $client['target_id'];
      }
      $default_users = $this->userStorage->loadMultiple($clients_ids);
    }

    $form['sidebar']['header']['user'] = [
      '#type' => 'entity_autocomplete',
      '#tags' => TRUE,
      '#title' => $this->t('Client:'),
      '#default_value' => $default_users,
      '#target_type' => 'user',
      '#selection_handler' => 'default:user_by_name',
      '#selection_settings' => [
        'include_anonymous' => FALSE,
        'filter' => [
          'type' => 'role',
          'role' => 'user',
        ],
      ],
    ];

    if (soundtact_user_is_admin($this->currentUser)) {
      // Get all themes.
      $term_list = $this->taxonomyStorage->loadTree('theme');
      $terms = [];

      foreach ($term_list as $term) {
        $terms[$term->tid] = $term->name;
      }

      $default_term_value = [];
      if ($route) {
        foreach ($route->field_theme as $theme) {
          $default_term_value[] = $theme->getValue()['target_id'];
        }
      }

      $form['sidebar']['header']['theme'] = [
        '#type' => 'select',
        '#title' => $this->t('Thema'),
        '#options' => $terms,
        '#multiple' => 'multiple',
        '#default_value' => $default_term_value,
      ];
    }

    // Create point reference.
    $form['sidebar']['main'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'sidebar-main',
        ],
      ],
    ];

    $points = $form_state->get('points');
    if ($points === NULL && $route) {
      $point_values = $route->get('field_points')->getValue();
      $points = $this->initPoints($form_state, $point_values);
    }

    $form['sidebar']['main']['points_fieldset'] = [
      '#tree' => TRUE,
      '#type' => 'fieldgroup',
      '#title' => $this->t('Added points'),
      '#prefix' => '<div id="points-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    // If the route has points render them else show no result message.
    if ($points && count($points) > 0) {

      foreach ($points as $key => $point) {
        $form['sidebar']['main']['points_fieldset'][] = $this->createPoint($key, $point);
      }
    }
    else {
      $form['sidebar']['main']['points_fieldset']['no_result'] = [
        '#type' => 'item',
        '#markup' => $this->t('No points found.'),
      ];
    }

    // This part need some explaination why i dit this. When you click on
    // a marker on the map I want to automatically add a reference field
    // to points-fieldset. I tried to do this with a custom Ajax call,
    // but had alot of trouble with creating this. That's why i choose
    // to create a button and just hide this button. When you click on
    // the marker a fake click on add_name will be done. The hidden field
    // is used to store the beacon id that needs to be added.
    $form['sidebar']['main']['map_beacon'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'edit-map-beacon',
      ],
    ];

    $form['sidebar']['main']['add_point'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addPoint'],
      '#submit_method' => 'editing',
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'wrapper' => 'sidebar-wrapper',
      ],
      '#attributes' => [
        'class' => [
          'sidebar-add-point',
        ],
      ],
    ];

    // Setup sidebar footer.
    $form['sidebar']['footer'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'sidebar-footer',
        ],
      ],
    ];

    $submit_title = $this->t('Create route');
    if ($editing) {
      $submit_title = $this->t('Save changes');
    }

    $form['sidebar']['footer']['button'] = [
      '#type' => 'submit',
      '#value' => $submit_title,
      '#submit_method' => 'saving',
    ];

    $canvas_id = Html::getUniqueId('soundtact-route-map');

    $form['main']['map_canvas'] = [
      '#type' => 'geolocation_map',
      '#centre' => [
        'lng' => 5.3104743,
        'lat' => 52.0466653,
      ],
      '#id' => $canvas_id,
      '#maptype' => 'leaflet',
      '#settings' => [
        'zoom' => 17,
        'map_features' => [
          'leaflet_control_geocoder' => [
            'enabled' => TRUE,
            'settings' => [
              'geocoder' => 'google_geocoding_api',
              'position' => 'topleft',
            ],
          ],
          'leaflet_marker_clusterer' => [
            'enabled' => TRUE,
            'settings' => [
              'cluster_settings' => [
                'show_coverage_on_hover' => '1',
              ],
              'disable_clustering_at_zoom' => 17,
            ],
          ],
        ],
      ],
      '#attached' => [
        'library' => [
          'soundtact_route/soundtact_route.map',
        ],
        'drupalSettings' => [
          'soundtact_route' => [
            'routeId' => $route_id,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Initialize points in the formstate to the form can render.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $points
   *   Array with points.
   *
   * @return array
   *   Returns the form_state points.
   */
  protected function initPoints(FormStateInterface $form_state, array $points) {
    $state_points = [];

    foreach ($points as $point) {
      $entity = $this->pointStorage->load($point['target_id']);

      // If these are not defined they will return null
      // otherwise it gives a int.
      $beacon_id = $entity->get('beacon')->getValue()[0]['target_id'];

      $beacon_name = $this->t('Name unknown');
      $beacon_location = ['lat' => 0, 'lng' => 0];
      if ($beacon_id !== NULL) {
        $beacon = $this->beaconStorage->load($beacon_id);
        if ($name = $beacon->name) {
          $beacon_name = $name->getValue()[0]['value'];
        }
        if ($location = $beacon->geolocation_field) {
          $beacon_location = $location->getValue()[0];
        }
      }

      $audio_id = NULL;
      $audio_name = '';
      $audio_url = '';
      if ($audio_reference_field = $entity->audio_reference) {
        $audio_id = (int) $audio_reference_field->getValue()[0]['target_id'];
        $audio = $this->mediaStorage->load($audio_id);
        $audio_name = $audio->name->getValue()[0]['value'];

        try {
          $audio_file_id = $audio->field_media_audio_file->getValue()[0]['target_id'];
          /** @var \Drupal\file\Entity\File $audio_file */
          $audio_file = $this->fileStorage->load($audio_file_id);
          $audio_url = $audio_file->get('uri')->getValue()[0]['value'];
        }
        catch (\Exception $exception) {
          $this->logger(self::MODULE_NAME)->error($exception->getMessage());
          $this->messenger->addMessage($this->t('Something went wrong when retrieving audio. Please try to refresh the page.', ['error']));
        }
      }

      $state_points[] = [
        'point_id' => $entity->id(),
        'beacon_name' => $beacon_name,
        'beacon_id' => $beacon_id,
        'audio_id' => $audio_id,
        'audio_name' => $audio_name,
        'audio_url' => $audio_url,
        'location' => $beacon_location,
      ];
    }

    $form_state->set('points', $state_points);
    return $state_points;
  }

  /**
   * Create a render object for Points.
   *
   * @param int $key
   *   The key of the point.
   * @param array $point
   *   Array with points values.
   *
   * @return array
   *   Render object.
   */
  protected function createPoint(int $key, array $point) {
    $point_render_object = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#access' => isset($point['removed']) ? FALSE : TRUE,
      '#attributes' => [
        'class' => [
          'point',
          'point-' . $point['point_id'],
        ],
      ],
    ];

    $point_render_object['title'] = [
      '#type' => 'label',
      '#title' => $point['beacon_name'],
      '#attributes' => [
        'class' => [
          'beacon-title',
        ],
        'data-point-key' => $key,
      ],
    ];

    $point_render_object['removed'] = [
      '#type' => 'hidden',
      '#value' => isset($point['removed']) ? TRUE : FALSE,
    ];

    $point_render_object['point_id'] = [
      '#type' => 'hidden',
      '#value' => $point['point_id'],
    ];

    $point_render_object['location'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $point_render_object['location']['lat'] = [
      '#type' => 'hidden',
      '#value' => $point['location']['lat'],
    ];

    $point_render_object['location']['lng'] = [
      '#type' => 'hidden',
      '#value' => $point['location']['lng'],
    ];

    $point_render_object['beacon_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => [
          'beacon-id',
        ],
      ],
      '#value' => $point['beacon_id'],
    ];

    $point_render_object['beacon_title'] = [
      '#type' => 'hidden',
      '#value' => $point['beacon_name'],
    ];

    $point_render_object['music_container'] = [
      '#prefix' => '<div class="music-add-wrapper">',
      '#suffix' => '</div>',
    ];

    $point_render_object['music_container']['music_name'] = [
      '#markup' => '<div class="music-title" data-audio-url="' . $point['audio_url'] . '">' . $point['audio_name'] . '</div>',
    ];

    $point_render_object['music_container']['music_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => [
          'point-music-id',
        ],
      ],
      '#value' => $point['audio_id'],
    ];

    $point_render_object['music_container']['music'] = [
      '#type' => 'submit',
      '#name'  => 'remove_' . $key,
      '#point_id' => $point['point_id'],
      '#submit' => [
        '::toggleDialog',
        '::setMusicPicker',
      ],
      '#ajax' => [
        'callback' => '::dialogCallback',
        'wrapper' => 'sidebar-wrapper',
      ],
      '#value' => $this->t('Change music'),
    ];

    $point_render_object['remove'] = [
      '#type' => 'submit',
      '#name'  => 'remove_' . $key,
      '#submit' => ['::removePoint'],
      '#point_key' => $key,
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'points-fieldset-wrapper',
      ],
      '#value' => 'X',
      '#attributes' => [
        'class' => [
          'remove-button',
        ],
      ],
    ];

    return $point_render_object;
  }

  /**
   * Set the theme of the dialog to musicPickerDialog.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function setMusicPicker(array &$form, FormStateInterface $form_state) {
    $form_state->set('current_changing_point', $form_state->getTriggeringElement()['#point_id']);
    $form_state->set('dialog_theme', MusicDialogFormHelper::$musicPickerDialog);
    $form_state->setRebuild();
  }

  /**
   * Add a new point to the form_state.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addPoint(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $beacon_id = $values['map_beacon'];
    $beacon = $this->beaconStorage->load($beacon_id);

    $beacon_name = $this->t('Name unknown');
    $beacon_location = ['lat' => 0, 'lng' => 0];
    if ($name = $beacon->name) {
      $beacon_name = $name->getValue()[0]['value'];
    }
    if ($location = $beacon->geolocation_field) {
      $beacon_location = $location->getValue()[0];
    }

    // We use  the existing createPoint to create a new point.
    // We do this because we need a point id to save new music to.
    // Without the point id it will work, but we will get bugs where
    // multiple points get edited, because they all have the same
    // point_id.
    $points = $this->savePoints([['beacon_id' => $beacon_id]]);

    $point_id = '';
    if (isset($points[0])) {
      $point_id = $points[0];
    }

    $point = [
      'point_id' => $point_id,
      'beacon_id' => $beacon_id,
      'beacon_name' => $beacon_name,
      'location' => $beacon_location,
      'audio_id' => NULL,
      'audio_name' => '',
      'audio_url' => '',
    ];

    $form_state_points = $form_state->get('points');
    $form_state_points[] = $point;
    $form_state->set('points', $form_state_points);
    $form_state->setRebuild();
  }

  /**
   * Check if beacons are already added.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   * @param int $beacon_id
   *   Id of the beacon.
   *
   * @return bool|int
   *   False when beacon doesn't exists otherwise the key.
   */
  protected function beaconExistsInPoint(FormStateInterface $form_state, int $beacon_id) {
    $existing_points = $form_state->get('points');
    if ($existing_points) {
      foreach ($existing_points as $key => $point) {
        if (((isset($point['removed']) && $point['removed '] === FALSE) || !isset($point['removed'])) && $point['beacon_id'] == $beacon_id) {
          return $key;
        }
        continue;
      }
    }
    return FALSE;
  }

  /**
   * Remove a point from the form_state.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function removePoint(array &$form, FormStateInterface $form_state) {
    $point_key_to_remove = $form_state->getTriggeringElement()['#point_key'];
    $points = $form_state->get('points');
    $points[$point_key_to_remove]['removed'] = TRUE;
    $form_state->set('points', $points);
    $form_state->setRebuild();
  }

  /**
   * Callback for ajax button.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed
   *   Form element.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['sidebar']['main']['points_fieldset'];
  }

  /**
   * Callback that is used to render the form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Form array.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['sidebar'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $trigger_element = $form_state->getTriggeringElement();

    if (isset($trigger_element['#submit_method'])) {
      // Validate form only when ajax call is invoked.
      if ($trigger_element['#submit_method'] === 'editing') {
        // First check if the beacon that is being added doesn't already exists.
        if ($values['map_beacon'] && !empty($values['map_beacon'])) {
          $beacon_id = $values['map_beacon'];
          $existing_key = $this->beaconExistsInPoint($form_state, $beacon_id);
          if ($existing_key !== FALSE) {
            $form_state->setErrorByName('points_fieldset', $this->t('The beacon is beign used by a point. Remove this point or add another beacon.'));
          }

          $beacon = $this->beaconStorage->load($beacon_id);
          if ($beacon === NULL) {
            $form_state->setErrorByName('map', $this->t('A error occured when adding a beacon to a point.'));
          }
        }
      }
      elseif ($trigger_element['#submit_method'] === 'saving') {
        // Check if the points aren't empty and
        // the audio field have a audio file.
        $points = $values['points_fieldset'];
        if (empty($points)) {
          $form_state->setErrorByName('points_fieldset', $this->t('Please add some points.'));
        }
        else {
          if (isset($points['no_result'])) {
            $form_state->setErrorByName('points_fieldset', $this->t('Please add a point'));
          }

          foreach ($points as $point) {
            if (!$point['removed'] && empty($point['music_container']['music_id'])) {
              $form_state->setErrorByName(
                'points_fieldset',
                $this->t('Please add a audio file for beacon: @beacon_name.', ['@beacon_name' => $point['beacon_title']])
              );
            }
          }
        }

        // Check if there is a default notification.
        $vibrations = $this->taxonomyStorage->loadTree('vibration');

        if (!isset($vibrations[0])) {
          $form_state->setErrorByName('vibration', $this->t('There is no default vibration. Please contact a administrator.'));
        }
        else {
          $form_state->set('default_vibration', $vibrations[0]);
        }

        $notifications = $this->taxonomyStorage->loadTree('notification');

        if (!isset($notifications[0])) {
          $form_state->setErrorByName('notification', $this->t('There is no default notification. Please contact a administrator.'));
        }
        else {
          $form_state->set('default_notification', $notifications[0]);
        }

        // Check if route_title is empty.
        if (empty($values['route_title'])) {
          $form_state->setErrorByName('route_title', $this->t('The title cannot be empty.'));
        }
      }
      elseif ($trigger_element['#submit_method'] === 'savingExistingMusic') {
        $this->validateExistingMusic($form, $form_state);
      }
      elseif ($trigger_element['#submit_method'] === 'savingNewMusic') {
        $this->validateNewMusic($form, $form_state);
      }

      // Theme should always have a term when you are administrator.
      if (soundtact_user_is_admin($this->currentUser)) {
        if (empty($values['theme'])) {
          $form_state->setErrorByName('theme', $this->t('A theme should be selected.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $default_notification = $form_state->get('default_notification');
    $default_vibration = $form_state->get('default_vibration');

    $themes = [];
    if ($values['theme'] !== NULL) {
      $themes = array_values($values['theme']);
    }

    // If this field is empty then we need to create a new route.
    // Otherwise edit the existing route.
    if (empty($values['route_id'])) {
      $new_points = $this->savePoints($values['points_fieldset'], $themes);
      $node = $this->nodeStorage->create([
        'type' => 'route',
        'title' => $values['route_title'],
        'field_clients' => $values['user'],
        'field_vibration' => $default_vibration->tid,
        'field_notification' => $default_notification->tid,
        'field_points' => $new_points,
      ]);

      if (soundtact_user_is_admin($this->currentUser)) {
        $node->field_theme = $values['theme'];
      }

      try {
        $node->save();
        $redirect_url = Url::fromRoute('view.route_overview.page_1');
        $form_state->setRedirectUrl($redirect_url);
        $this->messenger->addMessage($this->t('The route has succesfully been created.'), 'status');
      }
      catch (EntityStorageException $e) {
        $this->logger(self::MODULE_NAME)->error($e->getMessage());
      }
    }
    else {
      $route = $this->nodeStorage->load($values['route_id']);
      if ($route) {

        // First we try to edit the route.
        try {
          $route->set('title', $values['route_title']);
          $route->set('field_vibration', $default_vibration->tid);
          $route->set('field_notification', $default_notification->tid);
          $route->set('field_clients', $values['user']);

          if (soundtact_user_is_admin($this->currentUser)) {
            $route->set('field_theme', $themes);
          }

          $new_points = $this->savePoints($values['points_fieldset'], $themes);
          $route->set('field_points', $new_points);
          $route->save();
          $redirect_url = Url::fromRoute('view.route_overview.page_1');
          $form_state->setRedirectUrl($redirect_url);
          $this->messenger->addMessage($this->t('The route has succesfully been edited.'), 'status');
        }
        catch (\Exception $e) {
          $this->logger(self::MODULE_NAME)->error($e->getMessage());
          $this->messenger->addMessage($this->t('Something went wrong when editing the route. Please try again or contact a administrator.'), 'error');
          return;
        }
      }
    }
  }

  /**
   * Save points by a given array.
   *
   * @param array $points
   *   Points array.
   * @param array $themes
   *   Array with themes.
   *
   * @return array
   *   Array with new points.
   */
  protected function savePoints(array $points, array $themes = []) {
    $new_points = [];
    $points_to_remove = [];

    foreach ($points as $point) {
      $new_point = NULL;
      // Point already exists so we should just edit the values.
      if (!empty($point['point_id'])) {
        $new_point = $this->pointStorage->load($point['point_id']);

        // First check if we should remove this point.
        if ($point['removed']) {
          $points_to_remove[] = $new_point;
          continue;
        }

        $new_point->set('audio_reference', $point['music_container']['music_id']);

        $name = 'Point Beacon ' . $point['beacon_id'];
        if (!empty($point['music_container']['music_id'])) {
          $name = 'Point Beacon ' . $point['beacon_id'] . ' - Audio ' . $point['music_container']['music_id'];
        }

        $new_point->set('name', $name);

        // This check has been added because when you are a
        // admin and you change a routes theme the audio entities
        // needs to be changed too. Because other routes could use the music
        // in their route we duplicate the audio entity and change the theme.
        if (soundtact_user_is_admin($this->currentUser) && $themes !== NULL) {
          $duplicated_media = $this->mediaStorage->load($point['music_container']['music_id']);

          $duplicated_media_array = $duplicated_media->toArray();

          unset($duplicated_media_array['mid']);
          unset($duplicated_media_array['uuid']);
          unset($duplicated_media_array['vid']);
          unset($duplicated_media_array['revision_translation_affected']);
          unset($duplicated_media_array['revision_uid']);
          unset($duplicated_media_array['revision_log']);
          unset($duplicated_media_array['revision_timestamp']);

          $duplicated_media_array['field_theme'] = $themes;

          try {
            $new_media = $this->mediaStorage->create($duplicated_media_array);
            $new_media->save();
          }
          catch (EntityStorageException $e) {
            $this->logger(self::MODULE_NAME)->error($e->getMessage());
          }

          $new_point->set('audio_reference', $new_media->id());
        }
        try {
          $new_point->save();
        }
        catch (EntityStorageException $e) {
          $this->logger(self::MODULE_NAME)->error($e->getMessage());
        }
      }
      else {
        $name = 'Point Beacon ' . $point['beacon_id'];
        if (!empty($point['music_container']['music_id'])) {
          $name = 'Point Beacon ' . $point['beacon_id'] . ' - Audio ' . $point['music_container']['music_id'];
        }

        $new_point = $this->pointStorage->create([
          'name' => $name,
          'beacon' => [$point['beacon_id']],
        ]);

        try {
          $new_point->save();
        }
        catch (EntityStorageException $e) {
          $this->logger(self::MODULE_NAME)->error($e->getMessage());
        }
      }

      // Add the new point to a array so we can add the whole array
      // to the route.
      if ($new_point !== NULL) {
        $new_points[] = $new_point->id();
      }
    }

    // Remove the points.
    if (count($points_to_remove) > 0) {
      try {
        $this->pointStorage->delete($points_to_remove);
      }
      catch (EntityStorageException $e) {
        $this->logger(self::MODULE_NAME)->error($e->getMessage());
      }
    }

    return $new_points;
  }

  /**
   * Alter theme of a audio entity.
   *
   * An admin can override the theme with a field so we check
   * if a admin is editing the route so we can set the music
   * to this theme.
   *
   * @param array $point
   *   Point array.
   * @param array|null $themes
   *   The themes for the audio entity.
   */
  protected function alterMediaTheme(array $point, array $themes = NULL) {
    if (soundtact_user_is_admin($this->currentUser) && $themes !== NULL) {
      $media = $this->mediaStorage->load($point['music_container']['music_id']);
      $media->set('field_theme', $themes);
      try {
        $media->save();
      }
      catch (EntityStorageException $e) {
        $this->logger(self::MODULE_NAME)->error($e->getMessage());
      }
    }
  }

}
