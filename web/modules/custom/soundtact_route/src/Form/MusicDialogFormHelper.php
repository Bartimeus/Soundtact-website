<?php

namespace Drupal\soundtact_route\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Trait MusicDialogFormHelper.
 *
 * This trait is used to fill the dialog that is created in
 * DialogFormHelper. based on what is inside dialog_theme
 * we fill the dialog with different content.
 */
trait MusicDialogFormHelper {

  /**
   * Dialog theme name for music_picker.
   *
   * @var string
   *   Static constant.
   */
  public static $musicPickerDialog = 'music_picker_dialog';

  /**
   * Dialog theme name for new_music.
   *
   * @var string
   *   Static constant.
   */
  public static $newMusicDialog = 'new_music_dialog';

  /**
   * Dialog theme name for existing_music.
   *
   * @var string
   *   Static constant.
   */
  public static $existingMusicDialog = 'existing_music_dialog';

  /**
   * Dialog theme name.
   *
   * @var string
   *   Constant.
   */
  private $dialogThemeName = 'dialog_theme';

  /**
   * Id of the view to load.
   *
   * @var string
   *   Constant.
   */
  private $viewId = 'media_dialog';

  /**
   * Display id of the view to load.
   *
   * @var string
   *   Constant.
   */
  private $viewDisplayId = 'page_1';

  /**
   * Decide which dialog to open based on the dialog theme.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  private function setupMusicForm(array &$form, FormStateInterface $form_state) {
    $dialog_theme = $form_state->get($this->dialogThemeName);

    switch ($dialog_theme) {
      case self::$musicPickerDialog:
        $this->showMusicPicker($form, $form_state);
        break;

      case self::$newMusicDialog:
        $this->showNewMusic($form, $form_state);
        break;

      case self::$existingMusicDialog:
        $this->showExistingMusic($form, $form_state);
        break;

      default:
        break;
    }
  }

  /**
   * Show the musicPicker dialog content.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function showMusicPicker(array &$form, FormStateInterface $form_state) {
    $form['sidebar']['dialog']['content'] = DialogFormHelper::getBaseDialogContent();

    $form['sidebar']['dialog']['header']['title'] = [
      '#markup' => '<h2>' . $this->t('Choose music type') . '</h2>',
    ];

    $form['sidebar']['dialog']['content']['music_choose_wrapper'] = [
      '#prefix' => '<div id="music-choose-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['sidebar']['dialog']['content']['music_choose_wrapper']['new_music'] = [
      '#type' => 'submit',
      '#value' => $this->t('New music'),
      '#submit' => ['::changeToNewMusicTheme'],
      '#attributes' => [
        'class' => ['choose-music-button'],
      ],
      '#ajax' => [
        'callback' => '::dialogCallback',
        'wrapper' => 'sidebar-wrapper',
      ],
    ];

    $form['sidebar']['dialog']['content']['music_choose_wrapper']['existing_music'] = [
      '#type' => 'submit',
      '#value' => $this->t('Existing music'),
      '#submit' => ['::changeToExistingMusicTheme'],
      '#attributes' => [
        'class' => ['choose-music-button'],
      ],
      '#ajax' => [
        'callback' => '::dialogCallback',
        'wrapper' => 'sidebar-wrapper',
      ],
    ];
  }

  /**
   * Callback to rerender points fieldset.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   Array with points.
   */
  public function pointCallback(array &$form, FormStateInterface $form_state) {
    return $form['sidebar']['points_fieldset'];
  }

  /**
   * Callback to rerender dialog.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   Array with the dialog.
   */
  public function dialogMusicCallback(array &$form, FormStateInterface $form_state) {
    return $form['sidebar']['dialog'];
  }

  /**
   * Change the current theme to newMusicTheme.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function changeToNewMusicTheme(array &$form, FormStateInterface $form_state) {
    $form_state->set($this->dialogThemeName, self::$newMusicDialog);
    $form_state->setRebuild();
  }

  /**
   * Change the current theme to ExistingMusicTheme.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function changeToExistingMusicTheme(array &$form, FormStateInterface $form_state) {
    $form_state->set($this->dialogThemeName, self::$existingMusicDialog);
    $form_state->setRebuild();
  }

  /**
   * The content for newMusic Dialog.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function showNewMusic(array &$form, FormStateInterface $form_state) {
    $form['sidebar']['dialog']['content'] = DialogFormHelper::getBaseDialogContent();

    $form['sidebar']['dialog']['header']['title'] = [
      '#markup' => '<h2>' . $this->t('New music') . '</h2>',
    ];

    $form['sidebar']['dialog']['content']['new_music_wrapper'] = [
      '#prefix' => '<div class="new-music-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['sidebar']['dialog']['content']['new_music_wrapper']['new_music'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Music file'),
      '#description' => $this->t('This file is played when the client passes the point.'),
      '#upload_location' => 'public://audio/client/',
      '#upload_validators' => [
        'file_validate_extensions' => 'mp3 wav',
      ],
    ];

    $form['sidebar']['dialog']['content']['new_music_wrapper']['new_music_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title music file'),
      '#description' => $this->t('The title is used to find the music file.'),
    ];

    $form['sidebar']['dialog']['content']['new_music_wrapper']['music_submit'] = [
      '#type' => 'submit',
      '#submit_method' => 'savingNewMusic',
      '#value' => $this->t('Add music'),
      '#submit' => ['::saveNewMusic'],
      '#attributes' => [
        'class' => [
          'submit-new-music-button',
        ],
      ],
      '#ajax' => [
        'callback' => '::dialogCallback',
        'wrapper' => 'sidebar-wrapper',
      ],
    ];
  }

  /**
   * The content for newMusic dialog.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function showExistingMusic(array &$form, FormStateInterface $form_state) {
    $form['sidebar']['dialog']['content'] = DialogFormHelper::getBaseDialogContent();

    /** @var \Drupal\views\ViewExecutable $view */
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load($this->viewId)
      ->getExecutable();

    $form['sidebar']['dialog']['content']['view_wrapper'] = [
      '#prefix' => '<div class="media-view-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['sidebar']['dialog']['header']['title'] = [
      '#markup' => '<h2>' . $this->t('Existing music') . '</h2>',
    ];

    $excution_view_display = $view->executeDisplay($this->viewDisplayId);

    $form['sidebar']['dialog']['content']['view_wrapper']['view'] = [
      '#markup' => \Drupal::service('renderer')->render($excution_view_display),
    ];

    $form['sidebar']['dialog']['content']['selected_music'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['selected-music-input'],
      ],
    ];

    $form['sidebar']['dialog']['save']['submit'] = [
      '#prefix' => '<div class="view-submit-wrapper hidden">',
      '#suffix' => '</div>',
    ];

    $form['sidebar']['dialog']['save']['submit']['button'] = [
      '#type' => 'submit',
      '#submit_method' => 'savingExistingMusic',
      '#attributes' => [
        'class' => [
          'submit-existing-music-button',
        ],
      ],
      '#submit' => ['::saveExistingMusic'],
      '#ajax' => [
        'callback' => '::dialogCallback',
        'wrapper' => 'sidebar-wrapper',
      ],
      '#value' => $this->t('Save music'),
    ];
    $form['sidebar']['dialog']['content']['#attached']['library'] = ['soundtact_route/soundtact_route-view'];

    // We want to know which point to save the music to.
    $current_changing_point = (int) $form_state->get('current_changing_point');
    $form['sidebar']['dialog']['content']['#attached']['drupalSettings']['soundtact']['current_changing_point'] = $current_changing_point;
  }

  /**
   * Validate the new music form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateNewMusic(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if (!is_array($values['new_music']) || count($values['new_music']) <= 0) {
      $form_state->setErrorByName('new_music', $this->t('You must add new music file before saving.'));
    }
  }

  /**
   * Validate the existingMusic.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateExistingMusic(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Check if we are actually selected a value.
    if (empty($values['selected_music'])) {
      $form_state->setErrorByName('dialog][save][submit][button', $this->t('You need to select a music file before saving.'));
    }
  }

  /**
   * The save function for saving new music.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function saveNewMusic(array &$form, FormStateInterface $form_state) {
    $points = $form_state->get('points');
    $values = $form_state->getValues();
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $media_storage = $entity_type_manager->getStorage('media');
    $file_storage = $entity_type_manager->getStorage('file');
    $user_storage = $entity_type_manager->getStorage('user');
    $user = $user_storage->load(\Drupal::currentUser()->id());
    $user_themes = [];
    foreach ($user->field_theme->getValue() as $theme) {
      $user_themes[] = $theme['target_id'];
    }

    foreach ($points as &$point) {
      if ($point['point_id'] === $form_state->get('current_changing_point')) {
        $file_id = (int) $values['new_music'][0];
        $music_title = $values['new_music_title'];
        $music_file = $file_storage->load($file_id);
        if (empty($music_title)) {
          $music_title = $music_file->getFilename();
        }
        try {
          $new_media_array = [
            'bundle' => 'audio',
            'name' => $music_title,
            'field_media_audio_file' => [$file_id],
            'field_theme' => $user_themes,
          ];

          if (soundtact_user_is_admin($user)) {
            $new_media_array['field_theme'] = $values['theme'];
          }

          $media_storage->create($new_media_array);
          $new_media = $media_storage->create($new_media_array);
          $new_media->save();
        }
        catch (\Exception $exception) {
          \Drupal::logger('soundtact_route', $exception->getMessage());
          \Drupal::messenger()->addMessage($this->t('Something went wrong when saving the audio file. Please try again or contact a administrator.'), 'error');
          return;
        }
        $point['audio_id'] = $new_media->id();
        $point['audio_name'] = $music_title;
        $point['audio_url'] = $music_file->url();
      }
    }

    $form_state->set('dialog_open', FALSE);
    $form_state->set('dialog_theme', NULL);
    $form_state->set('points', $points);
    $form_state->setRebuild();
  }

  /**
   * The save function for saving existing music.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function saveExistingMusic(array &$form, FormStateInterface $form_state) {
    $points = $form_state->get('points');
    $values = $form_state->getValues();
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $media_storage = $entity_type_manager->getStorage('media');
    $file_storage = $entity_type_manager->getStorage('file');

    foreach ($points as &$point) {
      if ($point['point_id'] === $form_state->get('current_changing_point')) {
        $audio_id = (int) $values['selected_music'];
        $audio = $media_storage->load($audio_id);
        $media_name = $audio->name->getValue()[0]['value'];
        try {
          $audio_file_id = $audio->field_media_audio_file->getValue()[0]['target_id'];
          /** @var \Drupal\file\Entity\File $audio_file */
          $audio_file = $file_storage->load($audio_file_id);
          $point['audio_url'] = $audio_file->createFileUrl();
        }
        catch (\Exception $exception) {
          $this->logger('soundtact_route')->error($exception->getMessage());
          \Drupal::messenger()->addMessage($this->t('Something went wrong when retrieving audio. Please try to refresh the page.', 'error'));
        }
        $point['audio_id'] = $audio_id;
        $point['audio_name'] = $media_name;
      }
    }

    $form_state->set('dialog_open', FALSE);
    $form_state->set('dialog_theme', NULL);
    $form_state->set('points', $points);
    $form_state->setRebuild();
  }

}
