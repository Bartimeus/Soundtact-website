<?php

namespace Drupal\soundtact_route\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * This trait is created to  add dialog functionality to a custom form.
 */
trait DialogFormHelper {

  /**
   * The wrapper of the dialog.
   *
   * @var string
   *   Static constant.
   */
  public static $dialogWrapper = 'dialog-wrapper';

  /**
   * Get the base for the dialog content.
   *
   * This function has been created to make sure the dialog
   * always has the correct id.
   *
   * @return array
   *   Array with base of content.
   */
  public static function getBaseDialogContent() {
    return [
      '#prefix' => '<div id="dialog-content">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * Setup the base of the dialog.
   *
   * This  function needs to be added in the buildForm
   * function. This function will check if the form should be open
   * or closed and will add base things like closing and opening the
   * form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function setupDialog(array &$form, FormStateInterface $form_state) {
    $dialog_open = $form_state->get('dialog_open');
    $dialog_class = $dialog_open ? '' : 'dialog-hidden';

    $form['sidebar']['dialog'] = [
      '#prefix' => '<div id="' . self::$dialogWrapper . '" class="' . $dialog_class . '">',
      '#suffix' => '</div>',
    ];

    if ($form_state->get('dialog_open')) {

      $form['sidebar']['dialog']['header'] = [
        '#prefix' => '<div id="dialog-header">',
        '#suffix' => '</div>',
      ];

      $form['sidebar']['dialog']['header']['close'] = [
        '#type' => 'submit',
        '#submit' => ['::toggleDialog'],
        '#ajax' => [
          'callback' => '::dialogCallback',
          'wrapper' => 'sidebar-wrapper',
        ],
        '#value' => 'X',
        '#attributes' => [
          'class' => ['close-button'],
        ],
      ];

      $form['sidebar']['dialog']['content'] = self::getBaseDialogContent();

      $form['sidebar']['dialog']['header']['title'] = [
        '#markup' => '<h2>' . $this->t('Some title') . '</h2>',
      ];
    }
  }

  /**
   * Callback to rerender the dialog.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form object.
   */
  public function dialogCallback(array &$form, FormStateInterface $form_state) {
    return $form['sidebar'];
  }

  /**
   * Initialize the view when adding new music.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function toggleDialog(array &$form, FormStateInterface $form_state) {
    $form_state->set('dialog_open', $form_state->get('dialog_open') ? FALSE : TRUE);
    $form_state->set('dialog_theme', NULL);
    $form_state->setRebuild();
  }

}
