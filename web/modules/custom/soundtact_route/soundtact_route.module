<?php

/**
 * @file
 * Contains urbanbootcamp_routes.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function soundtact_route_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the soundtact_route module.
    case 'help.page.soundtact_route':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides route logic for Soundtact.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Alter the link of the title field for the route overview.
 *
 * @param array $vars
 *   Array with the view vars.
 */
function soundtact_route_preprocess_views_view_field(array &$vars) {
  if ($vars['view']->id() === 'route_overview' || $vars['view']->id() === 'client_route_overview') {
    if ($vars['field']->getField() == '.title') {
      $node = $vars['row']->_entity;
      $uri = new Url('soundtact.route.edit', ['route' => $node->id()]);
      $url = '/' . $uri->getInternalPath();
      $link = [
        '#markup' => '<a href="' . $url . '">' . $node->label() . '</a>',
      ];
      $vars['output'] = $link;
    }
  }
}

/**
 * Implements hook_entity_operation_alter().
 */
function soundtact_route_entity_operation_alter(array &$operations, EntityInterface $entity) {
  if (!(\Drupal::currentUser()->hasPermission('access content'))) {
    return;
  }

  $entityTypeId = $entity->getEntityTypeId();
  // Make sure we are modifying only node entity.
  if ($entityTypeId !== 'node') {
    return;
  }

  $nodeType = $entity->getType();
  $nodeId = $entity->id();
  // Make sure that we are modifying only album content type.
  if ($nodeType === 'route') {
    // Modify the url for album edit page, and use our own instead
    // of default drupal page.
    $editUrl = Url::fromRoute('soundtact.route.edit', ['route' => $nodeId]);
    $deleteUrl = Url::fromRoute('soundtact.route.delete', ['node' => $nodeId]);
    $operations['edit']['url'] = $editUrl;
    $operations['delete']['url'] = $deleteUrl;
  }
}

/**
 * This function is used to add the create a new entity button to views.
 *
 * We alter multiple views here, because it would be a waste of code
 * to copy this for every view.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The formstate.
 * @param string $form_id
 *   The  id of the form.
 */
function soundtact_route_form_views_exposed_form_alter(array &$form, FormStateInterface $form_state, string $form_id) {
  $view_ids = [
    'route_overview' => [
      'create_route' => 'soundtact.route.add',
      'button_title' => t('Create a new route'),
      'options' => [],
      'instruction' => 'Instructie video aanmaken route: <a href="https://bartimeus.sharepoint.com/portals/hub/_layouts/15/PointPublishing.aspx?app=video&p=p&chid=04f528dc-813a-4419-9d53-31fbd70e09c4&vid=be2f3be9-d3fc-4871-b1dd-3ecd5946915a&from=1" target="_blank">Klik hier!</a>',
    ],
    'client_overzicht' => [
      'create_route' => 'soundtact.client.add',
      'button_title' => t('Create a new client'),
      'options' => [],
      'instruction' => 'Instructie video aanmaken Cliënt: <a href="https://bartimeus.sharepoint.com/portals/hub/_layouts/15/PointPublishing.aspx?app=video&p=p&chid=04f528dc%2D813a%2D4419%2D9d53%2D31fbd70e09c4&vid=a8104d07%2D84a8%2D48a9%2D89e2%2D304ba5c5b19a&from=1" target="_blank">Klik hier!</a>',
    ],
    'user_overview' => [
      'create_route' => 'user.admin_create',
      'button_title' => t('Create a new user'),
      'options' => [],
    ],
    'thema_s' => [
      'create_route' => 'entity.taxonomy_term.add_form',
      'button_title' => t('Create a new Theme'),
      'options' => [
        'taxonomy_vocabulary' => 'theme',
      ],
    ],
    'audio_overview' => [],
  ];

  $view = $form_state->getStorage('view');

  if ($form_id == 'views_exposed_form' && isset($view_ids[$view['view']->id()])) {
    // Add create a new entity button.
    $button_details = $view_ids[$view['view']->id()];

    if (isset($button_details['create_route']) && isset($button_details['button_title'])) {
      $url = new Url($button_details['create_route'], $button_details['options']);
      $link = '/' . $url->getInternalPath();

      $form['actions']['new_entity'] = [
        '#markup' => '<a href="' . $link . '" class="button button-action button--primary" data-drupal-link-system-path="' . $link . '">' . $button_details['button_title'] . '</a>',
      ];
    }

    if (isset($button_details['instruction'])) {
      $form['instruction'] = [
        '#markup' => $button_details['instruction'],
      ];
    }

    // We also want to add a title to the filters.
    $view_title = $view['view']->getTitle();
    $form['view_title'] = [
      '#markup' => '<h2>' . $view_title . '</h2>',
      '#weight' => -1,
    ];

  }
}

/**
 * We don't want to show the back to home button in the toolbar.
 *
 * @param array $items
 *   Array with toolbar items.
 */
function soundtact_route_toolbar_alter(array &$items) {
  unset($items['home']);
}
