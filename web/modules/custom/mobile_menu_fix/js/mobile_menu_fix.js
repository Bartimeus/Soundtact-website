/**
 * @file
 */

(function ($, Drupal) {
  Drupal.behaviors.mobileMenuFix2 = {
    attach: function (context, settings) {
      $(document).ready(function () {
        // Close Admin menu on page load.
        var active_nav_icon = $('#toolbar-item-administration');
        if (
          active_nav_icon.length &&
          active_nav_icon.hasClass('is-active') &&
          Drupal.toolbar.models.toolbarModel.attributes.orientation === 'vertical'
        ) {
          active_nav_icon.click();
        }
      });
    }
  };
})(jQuery, Drupal);
