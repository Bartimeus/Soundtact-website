# UrbanBootcamp

This repository is the UrbanBootcamp application used by Bartiméus.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
Currently only tested on Mac Osx.

```
PHP 7.x
```

### Installing

This repository makes use of drupal-scaffolder, so you only need to run the following command to install the whole package:

```
composer install
```

And to start the server run the following command:

```
composer start-server
```

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Drupal](http://drupal.org/) - The Content Managerment System (CMS)
* [Composer](https://getcomposer.org/) - Dependency Management

## Authors

* **Niels Otten** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
